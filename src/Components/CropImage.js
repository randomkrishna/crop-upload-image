import React from 'react';
import Cropper from 'react-cropper';
import 'cropperjs/dist/cropper.css';

import './index.css';

class CropImage extends React.Component {
  state = {
    cropResult: null,
    reqWidth: 400,
    reqHeight: 400,
  };

  componentDidMount() {
    const { uuid = '', previewImages = [] } = this.props;
    if (uuid && previewImages) {
      const uuidFilter = previewImages.filter(p => parseInt(p.uuid) === parseInt(uuid)) || [];
      uuidFilter[0] &&
        this.setState({ reqWidth: uuidFilter[0].width, reqHeight: uuidFilter[0].height });
    }
  }

  _crop = e => {
    const { reqHeight, reqWidth } = this.state;
    if (typeof this.cropper.getCroppedCanvas() === 'undefined') {
      return;
    }

    if (e.detail.width !== reqWidth && e.detail.height !== reqHeight) {
      this.cropper.setData({ width: reqWidth, height: reqHeight, rotate: 0, scaleX: 1, scaleY: 1 });
    } else if (e.detail.width === reqWidth && e.detail.height === reqHeight) {
      this.setState({
        cropResult: this.cropper.getCroppedCanvas().toDataURL(),
      });
    }
    return;
  };

  render() {
    const { imgSrc = '', save, uuid } = this.props;
    const { cropResult } = this.state;

    return (
      <div>
        <Cropper
          ref={cropper => {
            this.cropper = cropper;
          }}
          src={imgSrc}
          style={{ height: 500, width: 500, margin: '20px auto 10px' }}
          guides={false}
          crop={this._crop}
          autoCrop={true}
          dragMode="move"
          zoomable={false}
          cropBoxMovable={true}
          cropBoxResizable={false}
        />
        <button className="button" onClick={() => save(uuid, cropResult)}>
          save
        </button>
      </div>
    );
  }
}

export default CropImage;
