import React from 'react';

export default function ImgCanvas({ src, width, height, name, uuid, onClickEdit }) {
  return (
    <div>
      {src && <img src={src} alt="" />}
      {name && (
        <p>
          {`${name} - ${width}x${height}  `}
          <span style={{ cursor: 'pointer', color: '#00afe8' }} onClick={() => onClickEdit(uuid)}>
            edit
          </span>
        </p>
      )}
    </div>
  );
}
