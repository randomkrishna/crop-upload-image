import React from 'react';
import './index.css';

export default function UploadArea({ src, onChange, imgError }) {
  return (
    <div className="file-upload-container">
      <div className="select-img-div">
        <label htmlFor="file-upload" className="custom-file-upload">
          Select New Image
        </label>
        <input id="file-upload" type="file" accept="image/*" name="file" onChange={onChange} />
        {imgError && <p className="error">Image does not have valid size of 1024x1024</p>}
      </div>
      <div className="preview-img-div ">
        {src && <p>Original Image</p>}
        {src && <img src={src} alt="" />}
      </div>
    </div>
  );
}
