import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Home from './Containers/Home';
import PreviousImageView from './Containers/PreviousImages';

function App() {
  return (
    <div className="App">
      <Router>
        <div>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/uploads/:id" component={PreviousImageView} />
          </Switch>
        </div>
      </Router>
    </div>
  );
}

export default App;
