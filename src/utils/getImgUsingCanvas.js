const getImgUsingCanvas = async (image, width, height, name) => {
  return new Promise((resolve, reject) => {
    const img = new Image();
    img.onload = function() {
      const canvas = document.createElement('canvas');
      canvas.width = img.width;
      canvas.height = img.height;
      const ctx = canvas.getContext('2d');
      ctx.drawImage(img, 0, 0);
      const imageData = ctx.getImageData(0, 0, width, height);

      const canvas1 = document.createElement('canvas');
      canvas1.width = width;
      canvas1.height = height;
      const ctx1 = canvas1.getContext('2d');
      ctx1.rect(0, 0, width, height);
      ctx1.fillStyle = 'white';
      ctx1.fill();
      ctx1.putImageData(imageData, 0, 0);
      resolve({ name, width, height, src: canvas1.toDataURL('image/png'), uuid: `${width}${height}`});
    };
    img.src = image;
  });
};

export default getImgUsingCanvas;
