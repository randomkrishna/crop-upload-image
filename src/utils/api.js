import axios from 'axios';
import { CLOUDI_API_KEY, CLOUDI_PRESET } from '../config';

export const uploadToCloudinary = image => {
  const data = new FormData();
  data.append('file', image);
  data.append('api_key', CLOUDI_API_KEY);
  data.append('upload_preset', CLOUDI_PRESET);
  return axios.post('https://api.cloudinary.com/v1_1/randomkrishna/image/upload', data, {
    headers: { 'X-Requested-With': 'XMLHttpRequest' },
  });
};
