export const updateLocalStorage = (name, data) => {
  localStorage.setItem(name, JSON.stringify(data));
};

export const getLocalStorage = (name) => JSON.parse(localStorage.getItem(name));