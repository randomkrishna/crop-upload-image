import React, { Component } from 'react';
import UploadArea from '../Components/UploadArea';
import CropperArea from '../Components/CropImage';
import { IMG_HEIGHT, IMG_WIDTH, IMG_SIZES, LOCALSTORAGE_NAME } from '../constants';
import ImagePr from '../Components/ImgCanvas';
import MaskBox from '../Components/MaskBox';
import { uploadToCloudinary } from '../utils/api';
import getImgUsingCanvas from '../utils/getImgUsingCanvas';
import { updateLocalStorage, getLocalStorage } from '../utils/localStorageHelper';
import { Link, withRouter } from 'react-router-dom';

import './index.css';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fileToUpload: null,
      imgError: false,
      previewImages: [],
      previousImages: [],
      showMask: false,
      showEdit: false,
      uuidToCrop: null,
    };
  }

  componentDidMount() {
    this.getPreviousImagesLocalStorage();
  }

  getPreviousImagesLocalStorage = () => {
    const previousImages = getLocalStorage(LOCALSTORAGE_NAME) || [];
    this.setState({ previousImages });
  };

  checkImageHeightWidth = async () => {
    const { fileToUpload } = this.state;
    const img = new Image();
    img.onload = async function() {
      const ImgSizesPromiseArr = Object.values(IMG_SIZES).map(size =>
        getImgUsingCanvas(fileToUpload, size.height, size.width, size.name)
      );
      const previewImages = await Promise.all(ImgSizesPromiseArr);
      previewImages && this.setState({ previewImages });
      this.setImageSize(img.width, img.height);
    }.bind(this);
    img.src = fileToUpload;
  };

  setImageSize = (width, height) => {
    if (width !== IMG_WIDTH && height !== IMG_HEIGHT) {
      this.setState({ fileToUpload: null, imgError: true, previewImages: [] });
      return;
    }
    this.setState({ width, height, imgError: false });
  };

  handleFileUpload = e => {
    const file = e.target.files[0];
    if (!file) return;
    const reader = new FileReader();
    reader.onload = e => {
      this.setState({ fileToUpload: e.target.result }, this.checkImageHeightWidth);
    };
    reader.readAsDataURL(file);
  };

  upload = () => {
    const { previewImages } = this.state;
    if (!previewImages.length) return;
    const d = new Date();
    const id = d.valueOf();
    const cloudinaryPromiseArr = previewImages.map(i => uploadToCloudinary(i.src));
    this.setState({ showMask: true });
    Promise.all(cloudinaryPromiseArr).then(res => {
      const imgUrls = res.map(r => {
        return { url: r.data.url, width: r.data.width, height: r.data.height };
      });
      const previousLS = getLocalStorage(LOCALSTORAGE_NAME) || [];
      updateLocalStorage(LOCALSTORAGE_NAME, [...previousLS, { id, imgUrls }]);
      this.props.history.push(`/uploads/${id}`);
    });
  };

  cropManually = uuid => {
    this.setState({ uuidToCrop: uuid, showEdit: true });
  };

  closeCropper = (uuid, src) => {
    const { previewImages } = this.state;
    const newPreviewImages = previewImages.map(p => {
      if (parseInt(uuid) === parseInt(p.uuid)) {
        return { ...p, src };
      } else {
        return { ...p };
      }
    });
    this.setState({ uuidToCrop: '', showEdit: false, previewImages: newPreviewImages });
  };

  render() {
    const {
      fileToUpload,
      imgError,
      previewImages,
      previousImages,
      showMask,
      showEdit,
      uuidToCrop,
    } = this.state;

    return (
      <>
        <MaskBox isVisible={showMask}>
          <h3>Uploading</h3>
        </MaskBox>
        {showEdit && (
          <MaskBox isVisible={showEdit}>
            <CropperArea
              imgSrc={fileToUpload}
              uuid={uuidToCrop}
              previewImages={previewImages}
              save={this.closeCropper}
            />
          </MaskBox>
        )}
        {!!previousImages.length && (
          <div className="previ-uploaded">
            <p>Previously Uploaded</p>
            <ul>
              {previousImages.map(p => (
                <li key={p.id}>
                  <Link to={`/uploads/${p.id}`}>
                    Upload On - {new Date(parseInt(p.id)).toUTCString()}
                  </Link>
                </li>
              ))}
            </ul>
          </div>
        )}
        <div>
          <UploadArea
            src={fileToUpload}
            onChange={this.handleFileUpload}
            imgError={imgError}
            imgRef={this.imgRef}
          />
        </div>
        <div>
          {previewImages.map(imageData => (
            <div className="align-center img-box" key={imageData.name}>
              <ImagePr {...imageData} onClickEdit={this.cropManually} />
            </div>
          ))}
        </div>
        <div className="align-center">
          {!!previewImages.length && (
            <button className="button" onClick={this.upload}>
              Upload
            </button>
          )}
        </div>
      </>
    );
  }
}

export default withRouter(Home);
