import React from 'react';
import { Link } from 'react-router-dom';
import { getLocalStorage } from '../utils/localStorageHelper';
import { LOCALSTORAGE_NAME } from '../constants';
import './index.css';

export default function PreviousImages({ match }) {
  const getPrevData = id => {
    const previousImages = getLocalStorage(LOCALSTORAGE_NAME) || [];
    const filterValue =
      Array.isArray(previousImages) &&
      previousImages.filter(p => parseInt(p.id) === parseInt(id) && p);
    return filterValue[0].imgUrls ? (
      <div>
        <p>Upload On - {new Date(parseInt(id)).toUTCString()}</p>
        {filterValue[0].imgUrls.map(i => (
          <div key={i.url} style={{ padding: 20 }}>
            <img src={i.url} alt={i.url} />
            <p>
              Resolution {i.width}x{i.height}
            </p>
          </div>
        ))}
      </div>
    ) : (
      <p>No image available</p>
    );
  };
  return (
    <>
      <div className="home-go">
        <Link to="/">Home</Link>
      </div>
      <div className="align-center">{getPrevData(match.params.id)}</div>;
    </>
  );
}
