export const IMG_WIDTH = 1024;

export const IMG_HEIGHT = 1024;

export const IMG_SIZES = {
  horizontal: { name: 'horizontal', height: 755, width: 450 },
  vertical: { name: 'vertical', height: 365, width: 450 },
  horizontal_small: { name: 'horizontal small', height: 365, width: 212 },
  gallery: { name: 'gallery', height: 380, width: 380 },
};

export const LOCALSTORAGE_NAME = 'crop_images';
